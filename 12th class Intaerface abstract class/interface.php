<?php

	//interface create

	interface School{
		public function mySchool();
	}
	interface College{
		public function myCollege();
	}
	interface Varsity{
		public function myVarsity();
	}
	class Teacher implements School,College,Varsity{
		public function __construct(){
			$this->mySchool();
			$this->myCollege();
			$this->myVarsity();
		}
		public function mySchool(){
			echo "I am a School teacher.<br/>";
		}
		public function myCollege(){
			echo "I am a College teacher.<br/>";
		}
		public function myVarsity(){
			echo "I am a Varsity teacher.<br/>";
		}
	}

	$teacher =new Teacher();

?>