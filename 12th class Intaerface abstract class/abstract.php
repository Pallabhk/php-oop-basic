

<?php

	//abstract class 

	abstract class Student{
		public $name;
		public $age;

		//Non abstract Method 
		public function details(){
			echo $this->name. " is " .$this->age . "years Old <br/>";
		}
		//Abstract method
		abstract public function School();

	}

	//Sub Class create

	class Boy extends Student{

		public function describe(){
			return parent::details(). "And I am a high School Student.<br/>";
		}
		//Non abstract Method override
		
		public function School(){
			return "I like read Story Book";
		}
	}

	$stu = new Boy();
	$stu->name ="Pallab";
	$stu->age  ="24";
	echo $stu->describe();
	echo $stu->School();
?>