
<?php

	class Calculation{
		/* Method Chaining 
		public $a =0;
		public $b =0;
		public $result;


		public function getValue($x,$y){
			$this->a =$x;
			$this->b =$y;
			return $this;
		}
		public function grtResult(){

			$this->result =$this->a * $this->b;
			return $this->result;
		}
		*/
		//Type Hinting

		public function getValue(array$x){

			foreach ($x as $key => $value) {
				echo $value[0];
				echo " : ";
				echo $value[1] * $value[2]."<br/>";
			}
		}
	}
?>