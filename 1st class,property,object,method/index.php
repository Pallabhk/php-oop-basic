<?php
	//This is class,property,object,method

	class Person{
		//property of class 
		public $name;
		public $age;

		//method
		public function personName(){
			echo "Person name is :".$this->name.'<br/>';
		}
		public function personAge($value){
			echo "Person age is :".$this->age=$value;

		}
		
	}
	//create object
		$personOne = new Person;
		$personOne->name="Akhy Moni";
		$personOne->personName();
		$personOne->personAge("18");
?>