<?php

	$dsn  ="mysql:dbname=user_data;host=localhost;";
	$user = "root";
	$pass ="";

	try{
		$pdo =new PDO($dsn, $user, $pass);
	}catch(PDOException $e){
		echo "Connection Faill....".$e->getMessage();
	}

	/*Update Data with placeholder value

		$id= 1;
		$skill="JAva ,Php,Physics";
		$sql  = "update tbl_user set skill=? where id=?";
		$stmt =$pdo->prepare($sql);
		$stmt->execute(array($skill ,$id));

	Update Data with bindParam value
		$id =1;
		$skill="Java,Php";

		$sql ="update tbl_user set skill=:skill where id=:id";
		$stmt =$pdo->prepare($sql);
		$stmt->bindParam(':skill',$skill);
		$stmt->bindParam(':id', $id);
		$stmt->execute();

	*/
		/*Delete Data */
		$id =12;

		$sql ="delete from tbl_user where id=?";
		$stmt=$pdo->prepare($sql);
		$stmt->execute(array($id));
	
		/*Insert data
		$name ="Moni Roy";
		$email="akhy@gmail.com";
		$skill="Micro-Biology";
		$age ="20";
		*/

	

	/*Array execute
	$sql ="insert into tbl_user(name, email, skill, age) values(?, ?, ?, ?)";
	$stmt= $pdo->prepare($sql);
	$arr = array($name,$email,$skill,$age);
	$stmt->execute($arr);

	*/


	/*BindParam diya execute
	bindParam only work on variable
	bindValue work both value and variable
	$sql ="insert into tbl_user(name, email, skill, age) values(:name, :email, :skill, :age)";

	$stmt = $pdo->prepare($sql);
	$stmt->bindParam(':name',$name, PDO::PARAM_STR);
	$stmt->bindParam(':email',$email, PDO::PARAM_STR);
	$stmt->bindParam(':skill',$skill, PDO::PARAM_STR);
	$stmt->bindParam(':age',$age, PDO::PARAM_INT);
	$stmt->execute();

	*/
?>