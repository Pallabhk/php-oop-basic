

<?php
	
	//traits example

	trait Java{

		public function javaCoder(){
			return "I love Java.</br>";
		}
	}

	trait Php{
		public function phpCoder(){
			return "I love PHP.";
		}
	}


    //Triat use as class

    trait JavaPhp{
    	use Java,Php;
    }
	//use multiple Class
	class CoderOne{
		use JavaPhp;
	}

	/*class CoderTwo{
		use Java;
	}

	*/

	$c1 = new CoderOne;
	echo $c1->javaCoder();

	// $c2 = new CoderTwo;
	echo $c1->phpCoder();
?>