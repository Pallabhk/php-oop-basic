
<?php

	class UserData{

		public $user;
		public $userId;
		//constant example
		const NAME ="Hora krishna Roy";
		//Static property call

		static $age ="30";

		public function __construct($userName, $userId){

			$this->user   =$userName;
			$this->userId =$userId;
			echo "Username is {$this->user} and user id is{$this->userId}";
		}
		public function __destruct(){
			unset($this->user);
			unset($this->userId);
		}
		//define contant 

		static function display(){
			//call constant value
			echo " Full name is ".UserData::NAME."<br/>";
			//call static property
			echo "Age is:".self::$age;
		}

	}
	$user ="Pallab";
	$id   ="01";
	$ur = new UserData($user,$id);

	echo "<br/>";
	UserData::display();


?>