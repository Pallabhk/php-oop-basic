<?php


	class Student{
		public $name;
		public $age;
		public $id;

		public function __construct($name ,$age){
			$this->name = $name;
			$this->age = $age;
		}
		public function setId($id){
			$this->id  = $id; 
		}

		public function __destruct(){
			if(!empty($this->id)){
				echo "Save student";
			}
		}

	}
	$stuOne =new Student("Akhy Moni","22");
	$stuOne->setId('12');
	unset($stuOne);
?>