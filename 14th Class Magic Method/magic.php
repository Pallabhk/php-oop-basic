<?php

	//Magic method 
/*

	__construct()
	__destruct()
	__get($property)
	__set($poperty, $Value)
	__call($method,$arg_array)
*/

	class Student{

		public function describe(){
			echo " I am a student.<br>";
		}

		//__get() method

		public function __get($am){
			echo "$am does not exist.<br/>";
		}

		//__set() method 
		public function __set($am ,$value){
			echo "we set $am->$value"."<br/>";
		}

		//__call Magic method

		public function __call($am , $value){

			echo "There is no <br>" .$am. 'method and Arguments:'.implode(', ', $value);
		}
	}


	$stu = new Student();
	$stu->describe();
	//undefine property
	$stu->pallab;
	//undefine Value
	$stu->age="15";

	$stu->notExistMethod('2','5','8');

?>