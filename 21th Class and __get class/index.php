<?php

	spl_autoload_register(function($class_name){
		include "class/" .$class_name. ".php";
	});

	class phpChild extends Php{
		public function cms(){
			echo "Base Class Constant and Class name->".__CLASS__."<br/>";
			echo "Base Class Function and Class name->".get_class($this)."<br/>";
		}
		public function myMethod(){

			parent::framework();
		}
	}

	$php = new phpChild();
	$php->framework();
	echo "<hr/>";
	$php->cms();
	echo "<hr/>";
	$php->myMethod();
?>